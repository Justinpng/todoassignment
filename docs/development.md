## Set up client side page
1. [HTML] Create template of HTML page - helps to determine endpoints
1. [Express] Set up express server file with boiler plate code (i.e. Require Express, body parser), routing to index page
```console.log("Starting..."); var express = require("express"); var app = express(); var bodyParser = require("body-parser"); app.use(bodyParser.urlencoded({ extended: false })); app.use(bodyParser.json()); const NODE_PORT = process.env.PORT || 3000; app.use(express.static(__dirname + "/../client/"));```
1. [HTML] Create routing using app.get ```app.get("/samplepage", function(req, res) {res.send("<h1>Sample Page</h1>")})```
1. Run nodemon to validate if server is running on localhost:3000

## Set up Angular file
1. [Angular] Set up the IIFE ```(function() {})();```
1. [Angular] Set up app Angular module, controller, and function. Module:```var app = angular.module("SampleApp", []);``` controller: ```app.controller("sampleCtrl", ["$http", sampleCtrl])```; function: ```function sampleCtrl($http)```
1. [Angular] Create init functions on server side app file. Angular: ```self.initForm = function() {$http.get(" ... ")}``` Express: ```app.get("/route", function(req, res) {...,res.json(output);}```
1. [HTML page] Define view models on HTML page. On HTML file: ```ng-model="ctrl.object.key"```. On Angular app file: ```self.object = { key: "value" }; ````
1. [Express/Angular] Define GET requests on express to execute init() functions that client has to see upon browser loading (i.e. using IIFE, init functions). On Express:```app.get("/route", function(req, res) {...});```
Angular : ```self.initForm = function() {$http.get("/popquizes").then(function(result) {...;}) ```

1. [Angular/Express] Define how POST/DELETE requests happen based on users events (i.e. how they are handled by Express functions)```if (checking.correctanswer == parseInt(quiz.value)){console.log("CORRECT !"); quiz.isCorrect = true }else{...;})```
1. [Express] Define the way that Express' logic returns data to client side upon each user event
1. [HTML/Angular] Define how view model changes with user events```{{ctrl.finalanswer.message}}```


to get angular set up  - html: include file, ng-app, ng-controller
on client side - app name, controller mane
$Scope = viewmodel