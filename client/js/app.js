// Always use an IIFE, i.e., (function() {})();
(function () {
    var app = angular.module("todoApp",[]);
    
    //Why?
    app.controller("TodoCtrl",["$http", TodoCtrl]);
    
    function TodoCtrl() {
        var vm = this;

        vm.todo = [];
        vm.doneTodo = [];
        vm.newTodo = {
            //taskName, priority, DueDate will populate on form submission here
        };

        //exposed functions
        vm.remove = remove;
        vm.submit = submit;
        vm.changeStatus = changeStatus;

        // ====
        // Function declaration and definition
        // These are private functions and become accessible only when exposed (attached to the vm object) as above
        
        //How is index capture? Through $index?
        function remove(doneStatus, index) {
            if(doneStatus) {
                vm.doneTodo.splice(index, 1);
            }
            else{
                vm.todo.splice(index, 1);
            }
        };

        function submit() {
            vm.todo.push(vm.newTodo);
            vm.newTodo = {};
        };

        //console.log of both arrays - todo and donetodo
        vm.showtodoarray = function(){
            console.log(vm.todo)
        }

        vm.showdonetodoarray = function(){
            console.log(vm.todo)
        }

        //Sort to do Items By Priority
        vm.sortToDoByPriority = function(){
            var newArray = []
            //Assign priority values for sorting
            for(var i=0 ; i < vm.todo.length; i++){
                if (vm.todo[i].priority == "HIGH") {
                    vm.todo[i].priorityValue = 1
                } else if (vm.todo[i].priority == "MEDIUM")  {
                    vm.todo[i].priorityValue = 2
                } else {
                    vm.todo[i].priorityValue = 3
                }
            }
            //Push HIGH items
            for (var i=0 ; i < vm.todo.length; i++){
                if (vm.todo[i].priorityValue == 1){
                    newArray.push(vm.todo[i])
                }
            };  
            //Push MEDIUM items  
            for (var i=0 ; i < vm.todo.length; i++){
                if (vm.todo[i].priorityValue == 2){
                    newArray.push(vm.todo[i])
                }
            };
            //Push LOW items
            for (var i=0 ; i < vm.todo.length; i++){
                if (vm.todo[i].priorityValue == 3){
                    newArray.push(vm.todo[i])
                }
            };
            vm.todo = newArray;
        }

        //Sort DONE to do Items By Priority
        vm.sortDoneToDoByPriority = function(){
            var newArray = []
            //Assign priority values for sorting
            for(var i=0 ; i < vm.doneTodo.length; i++){
                if (vm.doneTodo[i].priority == "HIGH") {
                    vm.doneTodo[i].priorityValue = 1
                } else if (vm.doneTodo[i].priority == "MEDIUM")  {
                    vm.doneTodo[i].priorityValue = 2
                } else {
                    vm.doneTodo[i].priorityValue = 3
                }
            }
            //Push HIGH items
            for (var i=0 ; i < vm.doneTodo.length; i++){
                if (vm.doneTodo[i].priorityValue == 1){
                    newArray.push(vm.doneTodo[i])
                }
            };  
            //Push MEDIUM items  
            for (var i=0 ; i < vm.doneTodo.length; i++){
                if (vm.doneTodo[i].priorityValue == 2){
                    newArray.push(vm.doneTodo[i])
                }
            };
            //Push LOW items
            for (var i=0 ; i < vm.todo.length; i++){
                if (vm.todo[i].priorityValue == 3){
                    newArray.push(vm.todo[i])
                }
            };
            vm.doneTodo = newArray;
        }
    
        //doneStatus == false, goes to todo array.
        //doneStatus == true, goes to donetodo array
        //NOTE: !doneStatus == true
        function changeStatus(doneStatus, index) {
            if(!doneStatus) {
                vm.doneTodo = vm.doneTodo.concat(vm.todo.splice(index, 1));
            }
            else {
                vm.todo = vm.todo.concat(vm.doneTodo.splice(index, 1));
                }
        }
    }
    
})();