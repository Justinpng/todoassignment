# Documentation for ToDoApp
Version: 1.0.0

## Capabilities
    * Add a new task
        * name of task
        * deadline for task
        * priority
    * Display the newly entered task
        * Default sorted by deadline
    * Sort task
        * by deadline
        * by priority
    * Handle status of task
        * Active
        * Completed
        * Deleted

## Project Members (and Slack alias)
    * Chan Hon Mun (munism13)
    * Chan Weng Wah (wengwah)
    * David Lam (davidlamyc)
    * Justin Png (justinpng)
    * Nick Sabai (nicksabai)
    * Tan Cheen Chong (cheen888)
    * Robin Xu (robinxu)

## File layout
    * index.html
        * display new task form
        * display active tasks
        * display completed tasks
    * client app.js
        * add new task
        * sort active tasks
        * act on task status
    * server app.js
        * accept new tasks submitted
        * maintain list of all tasks (active and completed)
        * update an existing task
        * delete an active task
